require 'compass-h5bp'
require 'susy'

http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"

relative_assets = true

output_style = :nested

line_comments = false

cache_dir = 'sass/cache'