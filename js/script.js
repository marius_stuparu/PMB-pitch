/* Author: Marius Stuparu */

jQuery(function($){
	/**
	 * Hook for the print button on header
	 */
	var printFunction = function() {
		$('#helpers').on('click', '#print', function(e){
			e.preventDefault()
			print()
		})
	}

	/**
	 * Add "more" symbols for items that have dropdown
	 */
	var navMenuFunction = function() {
		var $nav = $('nav'),
				$allListItems = $nav.find('li')

		// Add "plus" sign if the item contains a dropdown
		$allListItems.each(function(){
			var $that = $(this)

			if ( $that.find('> ul').length ) {
				$that.find('> a').append('<span class="icon plus"></span>')
			}
		})
	}

	/**
	 * Display weather in header widget
	 * Depends on jquery.simpleWeather and sugar.js
	 */
	var loadWeather = function() {
		var currentUtcDate = null, utcOffset = 0,
				currentDay = '', currentMonthNum = '', currentMonth = '', currentTime = ''

		currentUtcDate = new Date().utc(true)

		/**
		 * Adjust for daylight savings time
		 */
		if( currentUtcDate.isAfter('31 March 2013, 00:00') && currentUtcDate.isBefore('27 October 2013, 01:00') ) {
			utcOffset = 3
		} else {
			utcOffset = 2
		}
		currentUtcDate.addHours(utcOffset)

		currentDay = currentUtcDate.format('{dd}')
		currentMonthNum = currentUtcDate.format('{MM}')
		currentTime = currentUtcDate.format('{24hr}:{mm} EET')

		switch(currentMonthNum) {
			case '01': currentMonth = 'ianuarie'; break;
			case '02': currentMonth = 'februarie'; break;
			case '03': currentMonth = 'martie'; break;
			case '04': currentMonth = 'aprilie'; break;
			case '05': currentMonth = 'mai'; break;
			case '06': currentMonth = 'iunie'; break;
			case '07': currentMonth = 'iulie'; break;
			case '08': currentMonth = 'august'; break;
			case '09': currentMonth = 'septembrie'; break;
			case '10': currentMonth = 'octombrie'; break;
			case '11': currentMonth = 'noiembrie'; break;
			case '12': currentMonth = 'decembrie'; break;
		}

		/**
		 * AJAX call to Yahoo weather
		 */
		$.simpleWeather({
	    woeid: 868274, // http://woeid.rosselliot.co.nz/lookup/bucharest%20%20romania
	    unit: 'c',
	    success: function(weather) {
				var output = ''

				output =  '<div class="data">';
				output += '<a href="' + weather.link + '" target="_blank" title="Prognoza completă">';
				output += '<span class="degrees" data-tempF="' + weather.tempAlt + '"data-tempC="' + weather.temp + '">' + weather.temp + '&deg;C' + '</span>';
				output += '<span class="date">' + currentDay + '&nbsp;' + currentMonth + '</span>';
				output += '<span class="hour">' + currentTime + '</span>';
				output += '</a>'
				output += '</div>';
				output += '<a href="' + weather.link + '" target="_blank" title="Prognoza completă">';
				output += '<span class="image icon w' + weather.code + '"></span>'
				output += '</a>'
				output += '<p id="switch_degrees"><a href="#celsius" id="celsius" class="current">Celsius</a>&nbsp;|&nbsp;<a id="fahrenheit" href="#fahrenheit">Fahrenheit</a></p>'

        $("#weather").html(output);

        $('#switch_degrees').on('click', 'a', function(e){
					e.preventDefault();

					var $that = $(this),
							scale = $that.attr('id'),
							$display = $('#weather').find('.degrees'),
							degF = $display.attr('data-tempF'),
							degC = $display.attr('data-tempC')

					if( 'celsius' === scale ) {
						$display.html(degC + '&deg;C')
						$('#switch_degrees #celsius').addClass('current')
						$('#switch_degrees #fahrenheit').removeClass('current')
					} else {
						$display.html(degF + '&deg;F')
						$('#switch_degrees #fahrenheit').addClass('current')
						$('#switch_degrees #celsius').removeClass('current')
					}
				})
	    },
	    error: function(error) {
	        $("#weather").html('<p>'+error+'</p>');
	    }
		});
	}

	var setupSlider = function() {
		var $slider = $('#slider')

		$slider.anythingSlider({
			expand: true,
			buildArrows: true,
			buildNavigation: false,
			buildStartStop: false,
			appendBackTo: $('#arrow-left'),
			appendForwardTo: $('#arrow-right'),
			hashTags: false,
			autoPlay: true,
			delay: 8000
		})

		if( $slider.hasClass('dual') ) {
			$slider.on('click', 'a.daynight', function(e){
				e.preventDefault();

				var $that = $(this),
						$image_day = $that.parent('li').find('img.day'),
						$image_night = $that.parent('li').find('img.night')

				if( $that.hasClass('night') ) {
					$image_day.hide()
					$image_night.show()
					$that.removeClass('night').addClass('day')
				} else {
					$image_night.hide()
					$image_day.show()
					$that.removeClass('day').addClass('night')
				}
			})
		}
	}

	var setupGallery = function() {
		var $hero = $('.hero'),
				$galleries = $('.galleries'),
				basarab = [
					'img/gallery/basarab/basarab_01.jpg',
					'img/gallery/basarab/basarab_02.jpg',
					'img/gallery/basarab/basarab_03.jpg',
					'img/gallery/basarab/basarab_04.jpg',
					'img/gallery/basarab/basarab_05.jpg',
					'img/gallery/basarab/basarab_06.jpg',
					'img/gallery/basarab/basarab_07.jpg'
				],
				centrul_vechi = [
					'img/gallery/centrul-vechi/centrul-vechi_01.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_02.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_03.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_04.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_05.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_06.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_07.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_08.jpg',
					'img/gallery/centrul-vechi/centrul-vechi_09.jpg'
				],
				zile_bucuresti_2011 = [
					'img/gallery/zile-bucuresti-2011/zile-bucuresti-2011_01.jpg',
					'img/gallery/zile-bucuresti-2011/zile-bucuresti-2011_02.jpg',
					'img/gallery/zile-bucuresti-2011/zile-bucuresti-2011_03.jpg',
					'img/gallery/zile-bucuresti-2011/zile-bucuresti-2011_04.jpg',
					'img/gallery/zile-bucuresti-2011/zile-bucuresti-2011_05.jpg'
				],
				lumini_2012 = [
					'img/gallery/lumini-2012/lumini-2012_01.jpg',
					'img/gallery/lumini-2012/lumini-2012_02.jpg',
					'img/gallery/lumini-2012/lumini-2012_03.jpg',
					'img/gallery/lumini-2012/lumini-2012_04.jpg',
					'img/gallery/lumini-2012/lumini-2012_05.jpg',
					'img/gallery/lumini-2012/lumini-2012_06.jpg'
				],
				arena_nationala = [
					'img/gallery/arena-nationala/arena-nationala_01.jpg',
					'img/gallery/arena-nationala/arena-nationala_02.jpg',
					'img/gallery/arena-nationala/arena-nationala_03.jpg',
					'img/gallery/arena-nationala/arena-nationala_04.jpg',
					'img/gallery/arena-nationala/arena-nationala_05.jpg',
					'img/gallery/arena-nationala/arena-nationala_06.jpg',
					'img/gallery/arena-nationala/arena-nationala_07.jpg'
				],
				cismigiu = [
					'img/gallery/cismigiu/cismigiu_01.jpg',
					'img/gallery/cismigiu/cismigiu_02.jpg',
					'img/gallery/cismigiu/cismigiu_03.jpg',
					'img/gallery/cismigiu/cismigiu_04.jpg',
					'img/gallery/cismigiu/cismigiu_05.jpg',
					'img/gallery/cismigiu/cismigiu_06.jpg'
				],
				sediu_splai = [
					'img/gallery/sediu-splai/sediu-splai_01.jpg',
					'img/gallery/sediu-splai/sediu-splai_02.jpg',
					'img/gallery/sediu-splai/sediu-splai_03.jpg'
				],
				pedaland_2012 = [
					'img/gallery/pedaland-2012/pedaland-2012_01.jpg',
					'img/gallery/pedaland-2012/pedaland-2012_02.jpg',
					'img/gallery/pedaland-2012/pedaland-2012_03.jpg',
					'img/gallery/pedaland-2012/pedaland-2012_04.jpg',
					'img/gallery/pedaland-2012/pedaland-2012_05.jpg',
					'img/gallery/pedaland-2012/pedaland-2012_06.jpg',
					'img/gallery/pedaland-2012/pedaland-2012_07.jpg'
				],
				zile_bucuresti_2012 = [
					'img/gallery/zilele-bucurestiului-2012/zilele-bucurestiului-2012_01.jpg',
					'img/gallery/zilele-bucurestiului-2012/zilele-bucurestiului-2012_02.jpg',
					'img/gallery/zilele-bucurestiului-2012/zilele-bucurestiului-2012_03.jpg',
					'img/gallery/zilele-bucurestiului-2012/zilele-bucurestiului-2012_04.jpg',
					'img/gallery/zilele-bucurestiului-2012/zilele-bucurestiului-2012_05.jpg'
				],
				bucuresti_hd = [
					'img/gallery/bucuresti-hd/bucuresti-hd-01.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-02.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-03.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-04.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-05.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-06.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-07.jpg',
					'img/gallery/bucuresti-hd/bucuresti-hd-08.jpg'
				]

		$hero.on('click', 'a.fancybox', function(e){
			e.preventDefault();

			var $that = $(this),
					rel = $that.attr('rel'),
					gallery = []

			switch(rel) {
				case 'basarab': gallery = basarab; break;
				case 'centrul-vechi': gallery = centrul_vechi; break;
				case 'zile-bucuresti-2011': gallery = zile_bucuresti_2011; break;
				case 'lumini-2012': gallery = lumini_2012; break;
				case 'arena-nationala': gallery = arena_nationala; break;
				case 'cismigiu': gallery = cismigiu; break;
				case 'bucuresti-hd': gallery = bucuresti_hd; break;
			}

			$.fancybox.open(
				gallery, {
				nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 0,
        margin      : [20, 60, 20, 60]
      }
			)
		})

		$galleries.on('click', 'a.fancybox', function(e){
			e.preventDefault();

			var $that = $(this),
					rel = $that.attr('rel'),
					gallery = []

			switch(rel) {
				case 'cismigiu': gallery = cismigiu; break;
				case 'sediu-splai': gallery = sediu_splai; break;
				case 'pedaland-2012': gallery = pedaland_2012; break;
				case 'zile-bucuresti-2012': gallery = zile_bucuresti_2012; break;
			}

			$.fancybox.open(
				gallery, {
				nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 0,
        margin      : [20, 60, 20, 60]
      }
			)
		})
	}

	/**
	 * Main function
	 */
	var Init = function() {
		printFunction();
		navMenuFunction();
		loadWeather();

		if( $('#slider').length ) {
			// Load slider only if needed
			setupSlider();
		}

		if( $('body').hasClass('gallery') ) {
			// Load Fancybox on gallery pages
			setupGallery()
		}
	}

	Init()
})